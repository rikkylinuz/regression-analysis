# Regression Analysis

Project: House Prices: Advanced Regression Techniques<br>
file:  DS_Project_Final.ipynb

Preprocessing steps:
1. Columns with more than 80% NaN vales, same values are dropped
2. Converting Categorical to Numerical values
     i. Used Integer Mapping for some columns with similar values
    ii. Used Label Encoder
3. Missing values are populated using Iterative Imputer
4. Data is scaled using Standard Scaler
5. Features are selected using RFECV

1. Five Regression Models are applied with 10-Fold CV to the processed datasets
     i. LinearRegression
    ii. KNeighborsRegressor
   iii. CatBoostRegressor
    iv. GradientBoostingRegressor
     v. RandomForestRegressor
2. Models are evaluated with MAE, MSE, RMSE, R2

Used pycaret on the dataset and compared the results using r2 scores
